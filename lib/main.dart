import 'package:bloc/bloc.dart';
import 'package:flutter/widgets.dart';
import 'tictacteo_observer.dart';
import "package:new_iam/view/tictacteo_page.dart";

void main() {
  BlocOverrides.runZoned(
    () => runApp(const TicTacTeoPage()),
    blocObserver: TicTacTeoObserver(),
  );
}

