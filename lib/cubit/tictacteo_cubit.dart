import "package:bloc/bloc.dart";
import 'package:new_iam/cubit/cubit.dart';
import "package:new_iam/cubit/tictacteo_states.dart";

enum PlayersTurn { playerX, playerO, initial }

class TicTacTeoCubit extends Cubit<TicTacTeoStates> {
  TicTacTeoCubit() : super(InitialState());

  PlayersTurn playersTurn = PlayersTurn.playerX;
  //TODO remove this and add some nicer way to print the player
  String thePrintedTurn = "X";
  List<List<PlayersTurn>> boxes = [
    [PlayersTurn.initial, PlayersTurn.initial, PlayersTurn.initial],
    [PlayersTurn.initial, PlayersTurn.initial, PlayersTurn.initial],
    [PlayersTurn.initial, PlayersTurn.initial, PlayersTurn.initial]
  ];
  void changeTurn(int x, int y) {
    if (playersTurn == PlayersTurn.playerX) {
      boxes[x][y] = PlayersTurn.playerX;
      thePrintedTurn = "X";
      playersTurn = PlayersTurn.playerO;
    } else if (playersTurn == PlayersTurn.playerO) {
      boxes[x][y] = PlayersTurn.playerO;
      thePrintedTurn = "O";
      playersTurn = PlayersTurn.playerX;
    }
    emit(ChangePlayerTurn());
  }

  void reStateGame() {
    for (var i = 0; i < 3; i++) {
      for (var j = 0; j < 3; j++) {
        boxes[i][j] = PlayersTurn.initial;
      }
    }
    emit(ReStatrtedGame());
  }
}

