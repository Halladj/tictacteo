import 'dart:io';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import "package:new_iam/cubit/cubit.dart";

class GameBox extends StatelessWidget {
  GameBox({Key? key, required this.x, required this.y}) : super(key: key);
  bool isItUsed = false;
  int x;
  int y;
  @override
  Widget build(BuildContext context) {
    final TicTacTeoCubit cubit = BlocProvider.of<TicTacTeoCubit>(context);
    return BlocBuilder<TicTacTeoCubit, TicTacTeoStates>(
        builder: (context, state) {
      if (cubit.boxes[x][y] == PlayersTurn.initial) {
        return Container(
          height: 100.0,
          width: 100.0,
          margin: const EdgeInsets.all(10),
          child: NeumorphicButton(
            style: const NeumorphicStyle(
              color: Colors.purple,
              depth: 3.0,
              intensity: 3.0,
            ),
            onPressed: () {
              isItUsed = true;
              cubit.changeTurn(x, y);
            },
          ),
        );
      } else {
        if (cubit.boxes[x][y] == PlayersTurn.playerX) {
          return Container(
            height: 100.0,
            width: 100.0,
            margin: const EdgeInsets.all(10),
            child: NeumorphicButton(
                //onPressed: () {
                //  cubit.changeTurn();
                //},
                style: const NeumorphicStyle(
                  color: Colors.purple,
                  depth: 3.0,
                  intensity: 3.0,
                ),
                child: const Center(
                  child: Text(
                    "X",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 80),
                  ),
                )),
          );
        } else {
          return Container(
            height: 100.0,
            width: 100.0,
            margin: const EdgeInsets.all(10),
            child: NeumorphicButton(
              //onPressed: () {
              //  cubit.changeTurn();
              //},
              style: const NeumorphicStyle(
                color: Colors.purple,
                depth: 3.0,
                intensity: 3.0,
              ),
              child: const Center(
                child: Text(
                  "O",
                  style: TextStyle(fontSize: 80.0, fontWeight: FontWeight.bold),
                ),
              ),
            ),
          );
        }
      }
    });
  }
}

