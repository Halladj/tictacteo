import 'package:flutter/material.dart';
import 'package:new_iam/cubit/cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class GameAppBar extends StatelessWidget implements PreferredSizeWidget {
  final AppBar appBar;
  const GameAppBar({Key? key, required this.appBar}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final TicTacTeoCubit cubit = BlocProvider.of<TicTacTeoCubit>(context);
    return AppBar(
      title: const Text(
        "Tic Tac Toe",
        style: TextStyle(
            fontSize: 24.0, color: Colors.white, fontWeight: FontWeight.bold),
      ),
      backgroundColor: Colors.purple,
      leading: IconButton(
        icon: const Icon(Icons.border_all),
        iconSize: 30.0,
        color: Colors.white,
        onPressed: () {},
      ),
      actions: [
        IconButton(
          onPressed: () {
            cubit.reStateGame();
          },
          icon: const Icon(Icons.refresh_rounded),
          iconSize: 30.0,
          color: Colors.white,
          tooltip: "Refresh",
        ),
        IconButton(
          onPressed: () {},
          icon: const Icon(Icons.exit_to_app),
          iconSize: 30.0,
          color: Colors.white,
          tooltip: "Exit",
        ),
        PopupMenuButton(
            itemBuilder: (BuildContext context) => <PopupMenuEntry>[
                  const PopupMenuItem(
                    child: Text("General info"),
                  ),
                  const PopupMenuItem(
                    child: Text("Settings"),
                  ),
                ]),
      ],
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(appBar.preferredSize.height);
}

