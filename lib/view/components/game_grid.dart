import 'package:flutter/material.dart';
import "package:new_iam/view/components/game_box.dart";

class GameGrid extends StatelessWidget {
  const GameGrid({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GridView(
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 3,
      ),
      shrinkWrap: true,
      children: [
        GameBox(x: 0, y: 0),
        GameBox(x: 0, y: 1),
        GameBox(x: 0, y: 2),
        GameBox(x: 1, y: 0),
        GameBox(x: 1, y: 1),
        GameBox(x: 1, y: 2),
        GameBox(x: 2, y: 0),
        GameBox(x: 2, y: 1),
        GameBox(x: 2, y: 2),
      ],
    );
  }
}

