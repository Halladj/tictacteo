import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:new_iam/cubit/cubit.dart';
import 'package:new_iam/cubit/tictacteo_cubit.dart';
import 'package:new_iam/view/components/components.dart';
import "package:provider/provider.dart";

class TicTacTeoPage extends StatelessWidget {
  const TicTacTeoPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => TicTacTeoCubit(),
      child: Consumer<TicTacTeoCubit>(
        builder: (_, cubit, __) {
          return MaterialApp(
              title: "can u please work",
              home: Scaffold(
                  appBar: GameAppBar(
                    appBar: AppBar(),
                  ),
                  body: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const GameGrid(),
                      SizedBox(
                        width: 150,
                        height: 75,
                        child: Card(
                            color: Colors.black,
                            child: Center(
                              child: Text(
                                "${cubit.thePrintedTurn}' Turn",
                                style: const TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white),
                              ),
                            )),
                      )
                    ],
                  )));
        },
      ),
    );
  }
}

